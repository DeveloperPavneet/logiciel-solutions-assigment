<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::match(['get','post'],'/empty','AdminController@empty');

Route::get('/login','AdminController@login');
Route::post('/login','AdminController@postLogin');


Route::match(['get','post'],'logout','AdminController@logout');
Route::match(['get','post'],'admin/profile','AdminController@profile');

Route::middleware(['admin'])->group(function () {
	Route::group(['prefix' => 'admin'], function () {
		Route::match(['get','post'],'add-employee','AdminController@addEmployee');
		Route::match(['get','post'],'check-employee-email','AdminController@checkEmployeeEmail');
		Route::match(['get','post'],'employee-list','AdminController@employeeList');
		Route::match(['get','post'],'edit-employee/{id}','AdminController@edit');
		Route::match(['get','post'],'check-employee-email-edit','AdminController@checkEmployeeEmailEdit');
		Route::match(['get','post'],'change-status/{id}','AdminController@changeStatus');
		Route::match(['get','post'],'delete/{id}','AdminController@delete');

	});
});

Route::group(['prefix' => 'employee'], function () {
	Route::match(['get','post'],'dashboard','AdminController@Empdashboard');

	
});