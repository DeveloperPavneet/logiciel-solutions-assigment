 <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                    @if(Auth::user()->admin == 1)
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/add-employee')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Add Employee</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('/admin/employee-list')}}" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span class="hide-menu">Employee List</span></a></li>
                    @else
                         <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('employee/dashboard')}}" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span class="hide-menu">Dashboard</span></a></li>
                    @endif
                      
                    
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- ============================================================== -->
