@extends('layouts.admin.layout')
@section('title', 'Employee Dashboard')
@show
@section('content')		

   <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                            <div class="alert alert-{{ $msg }}">
                              <strong>{{ Session::get('alert-' . $msg) }}</strong>
                            </div>
                            @endif
                        @endforeach
                        <img src="{{asset('images/'.Auth::user()->image)}}" class="img-responsive">
                        </div>
                      
                    </div>
                   
                </div>
            </div>
           
   </div>

@stop
@section('scripts')
@parent
 <script type="text/javascript">
   
 </script>
@stop
