<!DOCTYPE html>
<html lang="en">
	<head>
		<title>@yield('title')</title>
		@section('style')
			@include('includes.admin.head')
		@show
	</head>
	<body>
		<div class="preloader">
	        <div class="lds-ripple">
	            <div class="lds-pos"></div>
	            <div class="lds-pos"></div>
	        </div>
	    </div>
	    <!-- ============================================================== -->
	    <!-- Main wrapper - style you can find in pages.scss -->
	    <!-- ============================================================== -->
	    <div id="main-wrapper">
			@include('includes.admin.header')
			@include('includes.admin.sidebar')
			@yield('content')
		</div>
		<div class="page-footer">
			@include('includes.admin.footer')
		</div>
		@section('scripts')
				@include('includes.admin.footerscripts')
		@show
	</body>
</html>