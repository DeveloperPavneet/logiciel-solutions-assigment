@extends('layouts.admin.layout')
@section('title', 'Add Employee')
@show
@section('content')		

   <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                            <div class="alert alert-{{ $msg }}">
                              <strong>{{ Session::get('alert-' . $msg) }}</strong>
                            </div>
                            @endif
                        @endforeach
                        <div class="card">
                        @if(!empty($employees['data']))
                            <?php $count = 1; ?>
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th scope="col">#</th>
                                  <th scope="col">Name</th>
                                  <th scope="col">Email</th>
                                  <th scope="col">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                              @foreach($employees['data'] as $key => $value)
                              <?php $id = Crypt::encrypt($value['id']); ?>
                                <tr>
                                  <th scope="row">{{$count}}</th>
                                  <td>{{$value['name']}}</td>
                                  <td>{{$value['email']}}</td>
                                  <td>
                                      <a href="{{url('/admin/edit-employee/'.$id)}}" class="btn btn-primary">Edit</a>
                                      @if($value['status'] == 1)
                                        <a href="{{url('/admin/change-status/'.$id)}}" class="btn btn-default">Deactive User</a>
                                      @else
                                        <a href="{{url('/admin/change-status/'.$id)}}" class="btn btn-success">Active User</a>
                                      @endif
                                      <a href="javascript:void(0);" class="btn btn-danger delBtn" data-toggle="modal" data-target="#dleteMod" data-id="{{$id}}">Delete</a>
                                  </td>
                                </tr>
                                <?php $count++; ?>
                              @endforeach
                               
                              </tbody>
                            </table>
                        @else
                            <h4>No Employees Found..</h4>
                        @endif
                        <?php echo $employee->render(); ?>
                        </div>
                    </div>
                </div>
            </div>
   </div>
<!-- The Modal -->
<div class="modal" id="dleteMod">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Confirmation Modal</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      Are You sure want to delete employee ?
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <a href="javascript:void(0)" class="delete btn btn-danger">Delete</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
@stop
@section('scripts')
@parent
 <script type="text/javascript">
    $(document).on('click','.delBtn',function(){
        var id = $(this).attr('data-id');
        var url = "{{url('')}}"+"/admin/delete/"+id;
        $("#dleteMod .delete").attr('href',url);
    });
 </script>
@stop
