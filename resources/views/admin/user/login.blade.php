<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<!-- <link href="{{asset('frontend/css/login/css.css')}}" rel="stylesheet" type="text/css"/> -->
<link href="{{asset('admin/css/login/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/css/login/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/css/login/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/css/login/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{asset('admin/css/login/select2.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/css/login/login-soft.css')}}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="{{asset('admin/css/login/components.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/css/login/plugins.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/css/login/layout.css')}}" rel="stylesheet" type="text/css"/>
<link id="style_color" href="{{asset('admin/css/login/default.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/css/login/custom.css')}}" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<style type="text/css">
    label.error{
        color: red !important;
        font-weight: bold;
    }
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="javascript:void(0);" style="text-decoration:none;">
    <!-- <img src="{{asset('frontend/img/logo3.png')}}" alt=""/> -->
    <h2>LOGO</h2>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<div class="content">
    <form class="login-form" action="{{url('login')}}" method="post" id="loginForm">
        {{csrf_field()}}
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }}">
              <strong>{{ Session::get('alert-' . $msg) }}</strong>
            </div>
              <!--   <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> -->
            @endif
        @endforeach
        <h3 class="form-title" style="color:black;">Login to your account</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>
            Enter any username and password. </span>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" style="border-color:black;" type="text" autocomplete="off" placeholder="Email" name="username"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" style="border-color:black;" type="password" autocomplete="off" placeholder="Password" name="password"/>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" style="background-color:#DE2910;" class="btn blue pull-right">
            Login <i class="fa fa-arrow-right" aria-hidden="true"></i>

            </button>
        </div>
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" action="{{url('/sendEmail/forgetpassword')}}" method="post">
        {{ csrf_field() }}
        <h3>Forget Password ?</h3>
        <p>
            Enter your e-mail address below to reset your password.
        </p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email1"/>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn">
            <i class="m-icon-swapleft"></i> Back </button>
            <button type="submit" class="btn blue pull-right">
            Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright" style="color:black;">
     
Copyrights © 2019, All Rights Reserved.

</div>

<script src="{{asset('admin/js/login/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/login/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/login/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/login/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/login/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/login/jquery.cokie.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('admin/js/login/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/login/jquery.backstretch.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('admin/js/login/select2/select2.min.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('admin/js/login/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/login/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/login/demo.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/login/login-soft.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('js/validation/jquery.validate.js')}}"></script>
<script src="{{asset('js/additional-methods.min.js')}}"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
  Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
  Login.init();
  Demo.init();
       // init background slide images
       $.backstretch([
        "{{asset('admin/images/BG-1.jpg')}}",
        // "{{asset('admin/images/BG-2.jpg')}}",
        // "{{asset('admin/images/BG-3.jpg')}}",
        ], {
          fade: 1000,
          duration: 8000
    }
    );

});
</script>

<script type="text/javascript">
    $("#loginForm").validate({    
    error:'span',
    rules:{
        "username":{
            required:true,
            email:true,
        },
        "password":{
            required:true,
            minlength: 4,
           
        },
    },
    messages:{
            "username":{
                email:"Enter Valid Email."
            },
            "password":{
                minlength: "Password Should be minimum of length four."
            }
    },


    
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>