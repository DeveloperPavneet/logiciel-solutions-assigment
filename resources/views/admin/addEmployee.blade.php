@extends('layouts.admin.layout')
@section('title', 'Add Employee')
@show
@section('content')		

   <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('/admin/add-employee')}}" id="addEmp">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))
                                <div class="alert alert-{{ $msg }}">
                                  <strong>{{ Session::get('alert-' . $msg) }}</strong>
                                </div>
                                @endif
                            @endforeach
                            {{csrf_field()}}
                                <div class="card-body">
                                    <h4 class="card-title">Add Employee</h4>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="fname" placeholder="Name Here" name="name">
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" id="lname" placeholder="Password Here" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Email</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" id="email" placeholder="Email Here" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Image</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="image" placeholder=" Here" name="image" accept="image/png, image/jpeg , image/jpeg">
                                        </div>
                                    </div>
                                
                                  
                                </div>
                                <div class="border-top text-center">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                      
                    </div>
                   
                </div>
            </div>
           
   </div>

@stop
@section('scripts')
@parent
 <script type="text/javascript">
    $("#addEmp").validate({    
        error:'span',
        rules:{
            "email":{
                required:true,
                email:true,
                remote:"{{url('')}}"+"/admin/check-employee-email"
            },
            "password":{
                required:true,
                minlength: 4,
               
            },
            "name":{
                required:true,
                maxlength:50,
            },
            "image":{
                required:true,
            }
        },
        messages:{
                "email":{
                    email:"Enter Valid Email.",
                    remote:"Email Already Exists"
                },
                "password":{
                    minlength: "Password Should be minimum of length four."
                }
        },

 
    
    });
 </script>
@stop
