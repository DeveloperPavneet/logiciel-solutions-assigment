<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Socialite;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use Redirect,Response;
use DB;
use Hash;
use Crypt;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;


class AdminController extends Controller
{    
    
    public function empty(){
           
        return view('admin.pages.empty');
    }   

    public function login()
    {
        return view('admin.user.login');
    }

    public function postLogin(Request $r)
    {
        $data  = $r->input();

        if(Auth::attempt(array('email' => $data['username'], 'password' => $data['password']))){
            if(Auth::user()->status == 1){
                if(Auth::user()->admin == 1){
                // superadmin
                return Redirect::to('/admin/add-employee')->with('alert-success','logged in successfully');
                }else{
                    // other admin

                    return Redirect::to('/employee/dashboard')->with('alert-success','logged in successfully');

                }
            }else{
                return Redirect::to('/login')->with('alert-danger','Please Wait for the Admin Approval.');
            }
         
        }else{
            return Redirect::to('/login')->with('alert-danger','Wrong Credentials.');

        }

    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('/login')->with('alert-success','Logout Successfully.');

    }
    public function addEmployee(Request $request)
    {
       if($request->isMethod('get')){
            return view('admin.addEmployee');

       }else{

            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);

            $save = User::create([
                        'name'      => $request->name,
                        'password'  => bcrypt($request->password),
                        'email'     => $request->email,
                        'image'     => $imageName,
                        'status'    => 0,
                        'admin'     => 0,

                    ]);
            return Redirect::back()->with('alert-success','Employee Created.');

       }
    }

    public function checkEmployeeEmail(Request $request)
    {   
        
        $user = User::where('email',$request->email)->where('admin','!=',1)->get()->toArray();
       
        if(!empty($user)){
            return "false";
        }else{
            return "true";
        }

    }

    public function employeeList()
    {
       
        $employee = User::where('admin','!=',1)->paginate(10);
        $employees = json_decode(json_encode($employee),true);

        return view('admin.employeeList',compact('employee','employees'));

    }

    public function edit($id, Request $request)
    {   
        $enid = Crypt::decrypt($id);
        $user = User::where('id',$enid)->first();
        if($request->isMethod('get')){

            return view('admin.editEmployee',compact('user','id'));

       }else{

            $imageName  = $user->image;
            $password   = $user->password;
            if(!empty($request->password)){
                $password = bcrypt($request->password);
            } 
            if ($request->hasFile('image')) {
                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images'), $imageName);
            }
          

            $update = User::where('id',$enid)->update([
                        'name'      => $request->name,
                        'password'  => $password,
                        'email'     => $request->email,
                        'image'     => $imageName,
                        'status'    => 0,
                        'admin'     => 0,

                    ]);
            return Redirect::to('admin/employee-list')->with('alert-success','Employee Updated.');

       }
    }

    public function checkEmployeeEmailEdit()
    {
       $id = Crypt::decrypt($_GET['id']); 
       $user = User::where('id','!=',$id)->where('email',$request->email)->where('admin','!=',1)->get()->toArray();
       
        if(!empty($user)){
            return "false";
        }else{
            return "true";
        }
    }

    public function changeStatus($id)
    {
        $id = Crypt::decrypt($id);
        $user = User::where('id',$id)->first();
        if($user->status == 1){
            User::where('id',$id)->update(['status'=>0]);
        }else{
            User::where('id',$id)->update(['status'=>1]);

        }
        return Redirect::to('admin/employee-list')->with('alert-success','Status Changed.');
    }

    public function delete($id)
    {
        $id = Crypt::decrypt($id);
        $user = User::where('id',$id)->delete();

        return Redirect::to('admin/employee-list')->with('alert-success','User Deleted.');
    }

    public function Empdashboard()
    {
        
        return view('employee.dashboard');
    }
}