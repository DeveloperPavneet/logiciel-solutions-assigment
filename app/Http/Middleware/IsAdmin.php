<?php

namespace App\Http\Middleware;
use Auth;
use Redirect,Response;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return redirect::to('/login')->with('alert-danger','Please Login First');
        }

        if(auth()->user()->admin == 1){
            return $next($request);
        }
        return redirect('/login')->with('alert-danger','You have not admin access');
    }
}
