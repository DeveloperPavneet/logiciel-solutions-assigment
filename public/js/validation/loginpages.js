var host = window.location.host;
var proto = window.location.protocol;
var ajax_url = proto+"//"+host+"/erp/";


							/*-------Register Form-------*/
$(document).ready(function(){
	$("#signup").validate({
	errorElement: 'span',
        errorClass: 'error',
		rules:{
			"metime_id":{
				required:true,
			},
			"uname":{
				required:true,
				minlength: 3
			},
			"email":{
				required:true,
				email:true,
				remote: ajax_url+'check-email',
			},
			"phone_number":{
				required:true,
				maxlength:12,
				digits:true
			},
			"address":{
				required:true
			},
			"password":{
				required:true,
				minlength:6,
			},
			"cpsw":{
				required:true,
				minlength:6,
				equalTo:"#password"
			},
			"term":{
				required:true
			}

		},
		messages:{
			"uname":{
				required:"Enter Your Name.",
				minlength:"Name must be of 3 character long." 
			},
			"email":{
				required:"Enter Email.",
				remote:"This email has been already assigned to a user."
			},
			"phone_number":{
				required:"Enter Mobile Number.",
				maxlength:"Mobile Number should not be greater than 12 digits.",
				digits:"Enter Correct Mobile Number."
			},
			"address":{
				required:"Enter your address."
			},
			"password":{
				required:"Enter a suitable password.",
				minlength:"Password must be 6 character long.",
			},
			"cpsw":{
				required:"Enter Your Password again.",
			},
			"term":{
				required:"Accept terms and policies."
			}

		}
	});
});	


							/*-------Login-form-------*/
$("#login").validate({
	errorElement: 'span',
    errorClass: 'error',
	rules:{
		"uname":{
			required:true,
			email:true
		},
		"psw":{
			required:true
		}
	},
	messages:{
		"uname":{
			required:"Enter Email To Login.",
			email:"Enter Correct Email."
		},
		"psw":{
			required:"Enter Your Password."
		}
	}
});

							/*-------Edit Profile Validation-------*/

$("#edit-profile").validate({
	errorElement: 'span',
    errorClass: 'error',
    rules:{
    	"fname":{
    		required:true,    		
    	},
        "lname":{
            required:true,            
        },
    	"phone":{
    		required:true
    	},
    	"address":{
    		required:true
    	}
    },
    messages:{
    	"fname":{
    		required:"Enter your name.",    	
    	},
        "lname":{
            required:"Enter your name.",            
        },
    	"phone":{
    		required:"Enter your mobile number."
    	},
    	"address":{
    		required:"Enter your address."
    	}
    }
});

							/*---------Change Password-------*/

$("#change-pass").validate({
	errorElement: 'span',
    errorClass: 'error',
    rules:{
    	"currentpassword":{
    		required:true,
    		remote: ajax_url+'check-old-password',
    	},
    	"newpass":{
    		required:true,
    		minlength:6
    	},
    	"cnewpass":{
    		required:true,
    		equalTo:"#newpass"
    	}
    },
    messages:{
		"currentpassword":{
    		required:"Enter your old password.",
    		remote:"Old password is wrong."
    	},
    	"newpass":{
    		required:"Enter new password.",
    		minlength:"Password must be 6 character long."
    	},
    	"cnewpass":{
    		required:"Enter confirm new Password.",
    	}    	
    }
});


							/*-------Add Inventory Js-------*/
$("#add-inventory1").validate({   
    errorPlacement: function(error, element) {
        if (element.attr("type") == "radio") {
            error.insertBefore(element);
            } 
        else {
            error.insertAfter(element);
        }
    }    ,
    rules:{
    	"radio":{
    		required:true
    	},
    	"quantity":{
    		required:true,
    		digits:true
    	},
    	"weight":{
    		required:true
    	},
    	"source":{
    		required:true
    	},
    	"warehouse":{
    		required:true
    	},
    	"location":{
    		required:true
    	},
    	address:{
    		required:true
    	}
    },
    messages:{
    	"radio":{
    		required:"Choose inventory type."
    	},
    	"quantity":{
    		required:"Please insert quantity.",
    		digits:"Enter correct quantity."
    	},
    	"weight":{
    		required:"Please enter weight."
    	},
    	"source":{
    		required:"Please enter a source."
    	},
    	"warehouse":{
    		required:"Please enter warehouse."
    	},
    	"location":{
    		required:"Please enter loction."
    	},
    	address:{
    		required:"Please enter address."
    	}
    },
    submitHandler: function(form){
        $("#ajax-loader").show();
		$.ajax({
			type:"POST",
			dataTYpe:"JSON",
			data:$("#add-inventory1").serialize(),
			url:ajax_url+'add-inventory1',
			success: function(data)
			{
				var id = data.id;
				$(".id").val(id);
				$(".nextstep").click();
					
			},
			error: function()
			{
				return false;
			},
			complete: function(){
			    $("#ajax-loader").hide();
			}
		})    
    }
});

$("#add-inventory2").validate({
    errorPlacement: function(error, element) {
        if (element.attr("type") == "radio") {
            error.insertBefore(element);
            } 
        else {
            error.insertAfter(element);
        }
    }    ,
    rules:{
    	"name":{
    		required:true
    	},
    	"truck_number":{
    		required:true
    	},
    	"truck_weight":{
    		required:true
    	},
    	"hours":{
    		required:true
    	}
    },
    messages:{
    	"name":{
    		required:"Please enter the name of the truck driver."
    	},
    	"truck_number":{
    		required:"Please enter the number of the truck."
    	},
    	"truck_weight":{
    		required:"Please enter the weight of the truck."
    	},
    	"hours":{
    		required:"Please enter the hours."
    	}
    },
    /*submitHandler: function(form){
		$.ajax({
			type:"POST",
			dataTYpe:"JSON",
			data:$("#add-inventory1").serialize(),
			url:ajax_url+'add-inventory1',
			success: function(data)
			{
				var id = data.id;
				$(".id").val(id);
				$(".nextstep").click();
					
			},
			error: function()
			{
				return false;
			}
		})    
    }*/
});

